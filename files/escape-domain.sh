#!/bin/bash

ESCAPED=$( echo "${1}" | sed 's/\./\\\\./g' ) 
echo "${ESCAPED}"
